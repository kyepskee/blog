use orgize::elements::Element as El;
use orgize::{Event, Org};
use serde::Serialize;
use std::collections::HashSet;
use std::ffi::OsStr;
use std::fs;
use std::ops::AddAssign;
use std::path::Path;
use tree_sitter_config::Config;
use tree_sitter_highlight::{HighlightEvent, Highlighter};
use tree_sitter_loader::Loader;

// type Text = Vec<TextElement>;

#[derive(Debug, Serialize)]
pub enum Element {
    Headline(String),
    Section(Text),
    SourceCode(Vec<FormattedString>),
    ListItem(Text),
}

#[derive(Debug, Serialize, Clone)]
pub enum TextElement {
    Link { url: String, desc: String },
    Bold(String),
    Code(String),
    Normal(String),
}

type Text = Vec<TextElement>;

#[derive(Debug, Serialize, Copy, Clone, PartialEq)]
pub struct Format {
    color: u8,
    bold: bool,
    cursive: bool,
    underlined: bool,
}

impl Format {
    fn new() -> Format {
        Format {
            color: 0,
            bold: false,
            cursive: false,
            underlined: false,
        }
    }

    fn color(color: u8) -> Format {
        let mut f = Format::new();
        f.color = color;
        f
    }

    fn bold(mut self) -> Format {
        self.bold = true;
        self
    }

    fn cursive(mut self) -> Format {
        self.cursive = true;
        self
    }

    fn underlined(mut self) -> Format {
        self.underlined = true;
        self
    }
}

trait ApplyFormat {
    fn apply_format(self, format: Format) -> FormattedString;
}

impl ApplyFormat for String {
    fn apply_format(self, format: Format) -> FormattedString {
        FormattedString { text: self, format }
    }
}

#[derive(Debug, Serialize, Clone)]
pub struct FormattedString {
    format: Format,
    text: String,
}

impl AddAssign<Self> for FormattedString {
    fn add_assign(&mut self, rhs: Self) {
        self.text += &rhs.text;
    }
}

#[derive(Debug, Serialize)]
pub struct Post {
    pub id: String,
    pub summary: String,
    pub title: String,
    pub elems: Vec<Element>,
}

#[derive(Debug, Serialize, Clone)]
pub struct PostOutline<'a> {
    pub id: &'a str,
    pub summary: &'a str,
    pub title: &'a str,
}

#[derive(PartialEq, Debug, Clone, Copy)]
enum State {
    Title,
    Summary,
    ListItem,
    Text,
}

struct PostBuilder {
    post: Post,
    state: State,
    text_elems: Text,
    is_bold: bool,
}

impl PostBuilder {
    fn new(id: String) -> Self {
        PostBuilder {
            post: Post::new(id),
            state: State::Text,
            text_elems: vec![],
            is_bold: false,
        }
    }

    fn init_loader(_: &()) -> Loader {
        let highlight_names = vec![
            "function",
            "type",
            "variable.builtin",
            "constant",
            "keyword",
            "string",
            "operator",
            "delimiter",
            "number",
            "function.special",
            "property",
            "label",
            "variable",
            "comment",
        ]
        .iter()
        .map(|x| x.to_string())
        .collect::<Vec<_>>();

        let mut loader = Loader::new().unwrap();

        let config = Config::load().unwrap();

        // let theme_config: tree_sitter_cli::highlight::ThemeConfig = config.get()?;
        loader.configure_highlights(&highlight_names);
        let loader_config = config.get().unwrap();
        loader.find_all_languages(&loader_config).unwrap();

        loader
    }

    fn start(&mut self, el: &El) {
        // println!("start: {:?}", *el);
        if self.state == State::Title {
            return;
        }

        let t = &();
        let loader = Self::init_loader(t);

        use orgize::elements::SourceBlock;
        match el {
            El::Keyword(kw) => {
                println!("kw {:?}", kw);
                if kw.key == "TITLE" {
                    self.post.title = kw.value.to_string();
                }
                if kw.key == "SUMMARY" {
                    self.post.summary = kw.value.to_string();
                }
            }
            El::SourceBlock(SourceBlock { contents, .. }) => {
                self.post.elems.push(Element::SourceCode(highlight_code(
                    &loader,
                    contents.to_string(),
                )));
            }
            El::Bold => {
                self.is_bold = true;
            }
            El::Code { value } => {
                self.text_elems.push(TextElement::Code(value.to_string()));
            }
            El::Title(title) => {
                self.state = State::Title;
                if title.raw == "Summary" {
                    self.state = State::Summary;
                } else {
                    self.post
                        .elems
                        .push(Element::Headline(title.raw.to_string()));
                }
            }
            El::Text { value } => {
                println!("state: {:?}, text: {}", self.state, value);
                if self.state == State::Summary {
                    // self.post.summary = value.to_string();
                    // self.state = State::Text;
                } else {
                    if self.is_bold {
                        self.text_elems.push(TextElement::Bold(value.to_string()));
                    } else {
                        self.text_elems.push(TextElement::Normal(value.to_string()));
                    }
                }
            }
            El::ListItem(_) => {
                self.state = State::ListItem;
            }
            El::Link(link) => {
                let url = link.path.to_string();
                let desc = link.desc.clone().unwrap_or_default().to_string();
                self.text_elems.push(TextElement::Link { url, desc });
            }
            _ => {
                println!("Warning! Unused el {:?}", el);
            }
        }
    }

    fn end(&mut self, el: &El) {
        println!("{:?}", el);
        match el {
            El::Title(_) => {
                self.state = State::Text;
            }
            El::ListItem(_) => {
                println!("end listitem, {:?}", self.text_elems);
                self.post
                    .elems
                    .push(Element::ListItem(self.text_elems.clone()));
                self.text_elems = vec![];
                self.state = State::Text;
            }
            El::Paragraph { .. } => {
                if self.state == State::Text && !self.text_elems.is_empty() {
                    self.post
                        .elems
                        .push(Element::Section(self.text_elems.clone()));
                    self.text_elems = vec![];
                }
            }
            El::Bold => {
                self.is_bold = false;
            }
            _ => {}
        }
        // println!("end: {:?}", *el);
    }
}

impl Post {
    fn new(id: String) -> Self {
        Post {
            id,
            title: "".to_string(),
            elems: vec![],
            summary: "".to_string(),
        }
    }

    pub fn read(path: &Path) -> Post {
        let id = path.file_stem().unwrap().to_str().unwrap().to_string();
        let mut builder = PostBuilder::new(id);

        let contents =
            fs::read_to_string(path).expect(&format!("Failed while reading {}", path.display()));
        let org = Org::parse(&contents);

        // FIXME: add actual error handling

        for ev in org.iter() {
            match ev {
                Event::Start(el) => builder.start(el),
                Event::End(el) => builder.end(el),
            }
        }

        builder.post
    }

    pub fn outline(&self) -> PostOutline {
        PostOutline {
            title: &self.title,
            id: &self.id,
            summary: &self.summary,
        }
    }
}

pub fn highlight_code(loader: &Loader, s: String) -> Vec<FormattedString> {
    let highlight_names = vec![
        "function",
        "type",
        "variable.builtin",
        "constant",
        "keyword",
        "string",
        "operator",
        "delimiter",
        "number",
        "function.special",
        "property",
        "label",
        "variable",
        "comment",
    ]
    .iter()
    .map(|x| x.to_string())
    .collect::<Vec<_>>();

    let (language, language_config) = loader
        .language_configuration_for_file_name(Path::new("test.cpp"))
        .unwrap()
        .unwrap();

    let highlight_config = language_config.highlight_config(language).unwrap().unwrap();

    let mut highlighter = Highlighter::new();
    let highlights = highlighter
        .highlight(&highlight_config, s.as_bytes(), None, |_| None)
        .unwrap();

    let mut stack: Vec<String> = Vec::new();
    let mut vec = Vec::new();

    let styles: Vec<(HashSet<String>, Format)> = vec![
        (["variable".to_string()].into(), Format::color(3)),
        (["comment".to_string()].into(), Format::new().cursive()),
        (["function".to_string()].into(), Format::color(2)),
        (["type".to_string()].into(), Format::color(1)),
        (["keyword".to_string()].into(), Format::color(4)),
    ];

    for event in highlights {
        // println!("{:#?}", event);
        match event.unwrap() {
            HighlightEvent::Source { start, end } => {
                // eprintln!(
                //     "{}: \"{}\"",
                //     stack.last().unwrap_or(&"none".to_string()),
                //     &s[start..end]
                // );
                let hname = stack.last().cloned().unwrap_or("".to_string());
                let format: Format = styles
                    .iter()
                    .find(|x| x.0.contains(&hname))
                    .map(|x| x.1)
                    .unwrap_or(Format::new());
                vec.push(s[start..end].to_string().clone().apply_format(format));
            }
            HighlightEvent::HighlightStart(s) => {
                // eprintln!("highlight style started: {:?}, {}", s, highlight_names[s.0]);
                stack.push(highlight_names[s.0].clone().to_string());
            }
            HighlightEvent::HighlightEnd => {
                stack.pop();
                // eprintln!("highlight style ended");
            }
        }
    }

    let mut res: Vec<FormattedString> = Vec::new();
    for fs in vec.clone() {
        if let Some(x) = res.last() {
            if x.format == fs.format {
                let l = res.len();
                res[l - 1] += fs;
            } else {
                res.push(fs);
            }
        } else {
            res.push(fs);
        }
    }

    res
}

pub fn read_posts(path: &Path) -> Vec<Post> {
    let dir = fs::read_dir(path).unwrap();
    dir.filter_map(|file| {
        // FIXME: add actual error handling
        let path = file.unwrap().path();

        if path.extension() == Some(OsStr::new("org")) {
            Some(Post::read(&path))
        } else {
            None
        }
    })
    .collect()
}
