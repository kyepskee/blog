#[macro_use]
extern crate rocket;

use figment::Figment;
use rocket::fs::{relative, NamedFile};
use rocket::{Config, State};
use std::collections::HashMap;
use std::fs::File;
use std::io::{BufRead, BufReader, Error, Write};
use std::iter::FromIterator;
use std::path::{Path, PathBuf};
use std::process::Command;

mod post;
use post::Post;
use post::PostOutline;

#[get("/")]
async fn index() -> Option<NamedFile> {
    NamedFile::open(Path::new("elm/dist/index.html")).await.ok()
}

#[get("/<file..>")]
async fn files(file: PathBuf) -> Option<NamedFile> {
    let mut path = Path::new(relative!("elm/dist/")).join(file);
    if path.is_dir() {
        path.push("index.html");
    }
    
    NamedFile::open(path).await .ok()
}

// struct Secret<'a>(&'a str);
// #[derive(Debug)]
// enum SecretError {
//     Missing,
//     Invalid,
// }

// #[rocket::async_trait]
// impl<'r> FromRequest<'r> for Secret<'r> {
//     type Error = SecretError;
//
//     async fn from_request(req: &'r Request<'_>) -> request::Outcome<Self, Self::Error> {
//         fn valid_key(s: &str) -> bool {
//             s == "HARD_TO_GUESS_PASSWORD" // FIXME: add an actual hard to guess password
//             unimplemented!()
//         }
//
//         match req.headers().get_one("x-api-key") {
//             None => Outcome::Failure((Status::BadRequest, SecretError::Missing)),
//             Some(key) if valid_key(key) => Outcome::Success(Secret(key)),
//             Some(_) => Outcome::Failure((Status::BadRequest, SecretError::Invalid)),
//         }
//     }
// }

// #[post("/git")]
// fn git(key: Secret) {
// git_pull();
// restart();
// }

#[get("/api/posts/<id>")]
fn post_route(posts: &State<Posts>, id: String) -> Option<String> {
    // serde_json::to_string(&post).ok()
    serde_json::to_string(&posts.posts[&id]).ok()
}

#[get("/api/posts/image/<id>")]
async fn image_route(id: String) -> Option<NamedFile> {
    let path = Path::new(relative!("posts")).join(&format!("{}.jpg", id)); // FIXME: sanitize

    // println!("path: {}", path.display());

    NamedFile::open(&path).await.ok()
}

#[get("/api/posts")]
fn posts_list_route(posts: &State<Posts>) -> Option<String> {
    serde_json::to_string(
        &posts
            .posts
            .values()
            .map(|post| post.outline())
            .collect::<Vec<_>>(),
    )
    .ok()
}

struct Posts {
    posts: HashMap<String, Post>,
}

fn compile_posts(posts: &Vec<Post>) -> std::io::Result<()> {
    for post in posts.iter() {
        let post = post.clone();
        let mut file = File::create(format!("out/{}.json", post.id))?;
        write!(file, "{}", serde_json::to_string(post)?)?;
    }

    // serde_json::to_string(&outlines(posts))?;
    // .expect("Couldn't serialize outlines")
    let mut outlines = File::create("out/_outlines.json")?;
    write!(
        outlines,
        "{}",
        serde_json::to_string(&posts.iter().map(|post| post.outline()).collect::<Vec<_>>())?
    )?;

    Ok(())
}

fn build() {
    Command::new("npm")
        .current_dir("elm/")
        .args(&["run", "build"])
        .spawn()
        .expect("Failed to build site");
}

#[launch]
fn rocket() -> _ {
    let figment = Figment::from(Config::figment());
    // .merge(Serialized::default("tls", 2));

    let path = Path::new(file!())
        .parent()
        .unwrap()
        .parent()
        .unwrap()
        .join("posts/");

    let posts = post::read_posts(&path);
    compile_posts(&posts);
    // build();
    let state = Posts {
        posts: HashMap::from_iter(posts.into_iter().map(|post| (post.id.clone(), post))),
    };

    rocket::custom(figment)
        .mount(
            "/",
            routes![index, posts_list_route, image_route, post_route, files],
        )
        .manage(state)
}
