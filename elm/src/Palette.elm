module Palette exposing (..)

import Element exposing (Color, rgb255)


type alias Palette =
    { bg : Color
    , darkerBg : Color
    , lighterBg : Color
    , color1 : Color
    , color2 : Color
    , color3 : Color
    , color4 : Color
    , neutral : Color
    }


palette : Palette
palette =
    { bg = rgb255 17 20 28
    , darkerBg = rgb255 13 16 22
    , lighterBg = rgb255 47 54 76
    , color1 = rgb255 83 44 144
    , color2 = rgb255 98 147 212
    , color3 = rgb255 62 135 105
    , color4 = rgb255 158 80 101
    , neutral = rgb255 235 245 255
    }
