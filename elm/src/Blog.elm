module Blog exposing (..)

import Array
import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import FormattedString exposing (Format, FormattedString)
import Helper exposing (..)
import OptimizedDecoder as Decode exposing (Decoder, bool, field, int, string)
import Palette exposing (..)


type alias Link =
    { url : String, desc : String }


type TextElement
    = Bold String
    | LinkElement Link
    | Code String
    | Normal String


type Elem
    = SourceCode (List FormattedString)
    | Headline String
    | Section (List TextElement)
    | ListItem (List TextElement)


type alias Blog =
    { id : String
    , summary : String
    , title : String
    , elems : List Elem
    }


type alias BlogOutline =
    { id : String
    , summary : String
    , title : String
    }


decode : Decoder Blog
decode =
    Decode.map4 Blog
        (field "id" string)
        (field "summary" string)
        (field "title" string)
        (field "elems" (Decode.list decodeElem))


decodeOutline : Decoder BlogOutline
decodeOutline =
    Decode.map3 BlogOutline
        (field "id" string)
        (field "summary" string)
        (field "title" string)


decodeLink : Decoder Link
decodeLink =
    Decode.map2 Link (field "url" string) (field "desc" string)


decodeTextElem : Decoder TextElement
decodeTextElem =
    Decode.oneOf
        [ Decode.map Normal (field "Normal" string)
        , Decode.map Bold (field "Bold" string)
        , Decode.map Code (field "Code" string)
        , Decode.map LinkElement (field "Link" decodeLink)
        ]


decodeElem : Decoder Elem
decodeElem =
    Decode.oneOf
        [ Decode.map Headline (field "Headline" string)
        , Decode.map SourceCode (field "SourceCode" (Decode.list decodeFormattedString))
        , Decode.map ListItem (field "ListItem" (Decode.list decodeTextElem))
        , Decode.map Section (field "Section" (Decode.list decodeTextElem))
        ]


decodeFormat : Decoder Format
decodeFormat =
    Decode.map4 Format
        (field "color" int)
        (field "bold" bool)
        (field "cursive" bool)
        (field "underlined" bool)


decodeFormattedString : Decoder FormattedString
decodeFormattedString =
    Decode.map2 FormattedString
        (field "format" decodeFormat)
        (field "text" string)


divider : Element msg
divider =
    el [ paddingEach { bottom = 0, left = 100, right = 100, top = 0 }, width fill ]
        (el
            [ height (px 1)
            , width fill
            , Background.color palette.color1
            ]
            none
        )


view : Blog -> Element msg
view blog =
    let
        title =
            paragraph [ Font.size 50, Font.bold, Font.center, width fill ] [ text blog.title ]

        showFormattedText ftxt =
            let
                colors =
                    Array.fromList [ palette.neutral, palette.color1, palette.color2, palette.color3, palette.color4 ]

                fontAttrs =
                    [ returnIf Font.semiBold ftxt.format.bold
                    , returnIf Font.italic ftxt.format.cursive
                    , returnIf Font.underline ftxt.format.underlined
                    ]
                        |> List.filterMap identity

                color =
                    Array.get ftxt.format.color colors
                        |> Maybe.withDefault palette.neutral
                        |> Font.color
            in
            el (color :: fontAttrs) (text ftxt.text)

        -- text ftxt.text
        showLine ftxts =
            row [] (List.map showFormattedText <| List.filter (\text -> not <| String.isEmpty text.text) ftxts)

        splitLeave sep str =
            let
                l =
                    FormattedString.split sep str
            in
            List.indexedMap
                (\i x ->
                    if i == List.length l - 1 then
                        x

                    else
                        FormattedString.append x sep
                )
                l

        lines ftxts =
            List.map (splitLeave "\n") ftxts
                |> flatten
                |> List.filter (\str -> not (FormattedString.isEmpty str))
                |> splitAfter (\ftxt -> String.endsWith "\n" ftxt.text)
                |> List.map showLine

        textElem e =
            case e of
                Bold s ->
                    el [ Font.semiBold ] (text s)

                Code s ->
                    el [ Border.rounded 3, Background.color palette.lighterBg, Font.family [ Font.monospace ], paddingXY 2 0 ] (text s)

                LinkElement { url, desc } ->
                    link [ Font.color palette.color2, mouseOver [ Font.color palette.color1 ] ] { url = url, label = text desc }

                Normal s ->
                    text s

        f elem =
            case elem of
                SourceCode s ->
                    row []
                        [ el [ height fill, width (px 2), Background.color palette.color1 ] none
                        , column [ Font.family [ Font.monospace ], paddingXY 7 5, spacing 7 ] (lines s)
                        ]

                Headline s ->
                    el [ Font.size 30, paddingEach { bottom = 10, top = 20, right = 0, left = 0 }, Font.semiBold ] (text s)

                ListItem s ->
                    row [ width fill ] [ el [ Font.size 20 ] (text "~ "), paragraph [] (List.map textElem s) ]

                Section s ->
                    paragraph [ spacing 8 ] (List.map textElem s)

        list =
            List.map f blog.elems

        els =
            [ title, divider ] ++ list
    in
    textColumn [ centerX, Font.size 18, width (fill |> maximum 1100), height fill, paddingXY 10 40, spacing 20 ]
    els
