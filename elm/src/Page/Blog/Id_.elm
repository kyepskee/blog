module Page.Blog.Id_ exposing (Data, Model, Msg, page)

import Blog exposing (Blog)
import DataSource exposing (DataSource)
import DataSource.File
import DataSource.Http
import Head
import Head.Seo as Seo
import OptimizedDecoder as D
import Page exposing (Page, PageWithState, StaticPayload)
import Pages.PageUrl exposing (PageUrl)
import Pages.Secrets as Secrets
import Pages.Url
import Shared
import View exposing (View)


type alias Model =
    ()


type alias Msg =
    Never


type alias RouteParams =
    { id : String }


page : Page RouteParams Data
page =
    Page.prerender
        { head = head
        , routes = routes
        , data = data
        }
        |> Page.buildNoState { view = view }


routes : DataSource (List RouteParams)
routes =
    DataSource.map (List.map (\outline -> RouteParams outline.id))
        (DataSource.File.jsonFile (D.list Blog.decodeOutline) "../out/_outlines.json")


data : RouteParams -> DataSource Data
data routeParams =
    DataSource.map Data
        -- (DataSource.Http.get (Secrets.succeed <| "../out/" ++ routeParams.id) Blog.decode)
        (DataSource.File.jsonFile Blog.decode ("../out/" ++ routeParams.id ++ ".json"))


head :
    StaticPayload Data RouteParams
    -> List Head.Tag
head static =
    Seo.summary
        { canonicalUrlOverride = Nothing
        , siteName = static.data.blog.title
        , image =
            { url = Pages.Url.external "TODO"
            , alt = ""
            , dimensions = Nothing
            , mimeType = Nothing
            }
        , description = static.data.blog.summary
        , locale = Nothing
        , title = static.data.blog.title
        }
        |> Seo.website


type alias Data =
    { blog : Blog
    }


view :
    Maybe PageUrl
    -> Shared.Model
    -> StaticPayload Data RouteParams
    -> View Msg
view maybeUrl sharedModel static =
    { title = static.data.blog.title
    , body = [ Blog.view static.data.blog ]
    }
