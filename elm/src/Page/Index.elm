module Page.Index exposing (Data, Model, Msg, page)

import Blog exposing (Blog, BlogOutline)
import DataSource exposing (DataSource)
import DataSource.File
import DataSource.Http
import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Events exposing (..)
import Element.Font as Font
import Element.Input as Input
import FormattedString
import Head
import Head.Seo as Seo
import Helper exposing (..)
import Html.Attributes
import OptimizedDecoder as D
import Page exposing (Page, StaticPayload)
import Pages.PageUrl exposing (PageUrl)
import Pages.Secrets as Secrets
import Pages.Url
import Palette exposing (palette)
import Path exposing (Path)
import Route exposing (Route)
import Shared
import View exposing (View)


type alias Model =
    ()


type alias Msg =
    Never


type alias RouteParams =
    {}


page : Page RouteParams Data
page =
    Page.single
        { head = head
        , data = data
        }
        |> Page.buildNoState { view = view }


data : DataSource Data
data =
    DataSource.map Data
        -- (DataSource.Http.get (Secrets.succeed "http://127.0.0.1:8000/api/posts") (D.list Blog.decodeOutline))
        (DataSource.File.jsonFile (D.list Blog.decodeOutline) "../out/_outlines.json")


head :
    StaticPayload Data RouteParams
    -> List Head.Tag
head static =
    Seo.summary
        { canonicalUrlOverride = Nothing
        , siteName = "Hubert's blog"
        , image =
            { url = Pages.Url.external "TODO"
            , alt = ""
            , dimensions = Nothing
            , mimeType = Nothing
            }
        , description = ""
        , locale = Nothing
        , title = "Hubert's blog" -- metadata.title -- TODO
        }
        |> Seo.website


type alias Data =
    { posts : List BlogOutline }


divider =
    el [ paddingEach { bottom = 0, left = 0, right = 0, top = 0 }, width (px 200) ]
        (el
            [ height (px 1)
            , width fill
            , Background.color palette.color1
            ]
            none
        )


footer : Element msg
footer =
    row
        [ width fill
        , padding 10
        , Background.color palette.color1
        , Border.widthEach { top = 1, bottom = 0, left = 0, right = 0 }
        , Border.color palette.color2
        ]
        [ column [ alignRight, spacing 10 ]
            [ el [ alignRight ] <| text "Services"
            , el [ alignRight ] <| text "About"
            , el [ alignRight ] <| text "Contact"
            ]
        ]


modular : Int -> Int
modular x =
    round <| 16 * 1.5 ^ toFloat x



--
-- radius : Int -> Attribute msg
-- radius px =
--     Html.Attributes.style "border-radius" (String.fromInt px ++ "px")
--     |> htmlAttribute


viewList : List BlogOutline -> Element msg
viewList posts =
    let
        title t =
            paragraph [ Font.size (modular 2), Font.semiBold, width fill ] [ text t ]

        viewOutline outline =
            link []
                { url = "/blog/" ++ outline.id
                , label =
                    row [ spacing 20, paddingXY 0 20 ]
                        [ el [ height fill, width fill, Border.rounded 20, clip ]
                            (image [ width fill, height fill ] { src = "/api/posts/image/" ++ outline.id, description = "" })
                        , column [ width (fillPortion 2), spacing 8 ]
                            [ title outline.title

                            -- , el [ Font.size <| modular -1, Font.light ] (text "1.1.1970")
                            , el [ Font.size <| modular 0, paddingXY 0 5 ] (paragraph [] [ text outline.summary ])
                            ]
                        ]
                }
    in
    textColumn [ centerX, Font.size 16, width (fill |> maximum 1000), height fill, paddingXY 100 40, spacing 20 ]
        (List.map viewOutline posts)



-- (List.intersperse divider (List.map viewOutline posts))


view :
    Maybe PageUrl
    -> Shared.Model
    -> StaticPayload Data RouteParams
    -> View Msg
view maybeUrl sharedModel static =
    { title = ""
    , body = [ viewList static.data.posts ]
    }
