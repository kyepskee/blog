module Shared exposing (Data, Model, Msg(..), SharedMsg(..), template)

import Array
import Blog exposing (Blog)
import Browser.Navigation
import DataSource
import DataSource.Http
import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Events exposing (..)
import Element.Font as Font
import Element.Input as Input
import FormattedString
import Helper exposing (..)
import Html exposing (Html)
import Html.Attributes as Attr
import Json.Decode as D
import Pages.Flags
import Pages.PageUrl exposing (PageUrl)
import Pages.Secrets as Secrets
import Palette exposing (palette)
import Path exposing (Path)
import Route exposing (Route)
import SharedTemplate exposing (SharedTemplate)
import View exposing (View)


template : SharedTemplate Msg Model Data msg
template =
    { init = init
    , update = update
    , view = view
    , data = data
    , subscriptions = subscriptions
    , onPageChange = Just OnPageChange
    }


type Msg
    = OnPageChange
        { path : Path
        , query : Maybe String
        , fragment : Maybe String
        }
    | SharedMsg SharedMsg


type alias Data =
    ()


type SharedMsg
    = NoOp


type alias Model =
    { showMobileMenu : Bool
    }


init :
    Maybe Browser.Navigation.Key
    -> Pages.Flags.Flags
    ->
        Maybe
            { path :
                { path : Path
                , query : Maybe String
                , fragment : Maybe String
                }
            , metadata : route
            , pageUrl : Maybe PageUrl
            }
    -> ( Model, Cmd Msg )
init navigationKey flags maybePagePath =
    ( { showMobileMenu = False }
    , Cmd.none
    )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        OnPageChange _ ->
            ( { model | showMobileMenu = False }, Cmd.none )

        SharedMsg globalMsg ->
            ( model, Cmd.none )


subscriptions : Path -> Model -> Sub Msg
subscriptions _ _ =
    Sub.none


data : DataSource.DataSource Data
data =
    DataSource.succeed ()


icon : Element msg
icon =
    el [] none


navbar : Bool -> Element msg
navbar hovered =
    let
        buttons =
            List.map
                (\x -> el [ alignRight ] x)
                []

        -- [ text "Home", text "Blog" ]
        title =
            -- link [ onMouseEnter (Hover True TitleHover), onMouseLeave (Hover False TitleHover) ]
            link []
                { url = "/"
                , label =
                    el
                        (if hovered then
                            [ Font.underline ]

                         else
                            []
                        )
                        (text "Hubert's Blog")
                }

        welcome =
            row [] [ icon, title ]

        bottomBorder w =
            Border.widthEach
                { bottom = w
                , top = 0
                , left = 0
                , right = 0
                }

        borderOptions =
            [ Border.shadow { blur = 3, color = palette.darkerBg, offset = ( 0, 0 ), size = 2 }
            ]
    in
    row
        ([ width fill
         , spacing 50
         , padding 20
         ]
            ++ borderOptions
        )
        (welcome :: buttons)


divider : Element msg
divider =
    el [ paddingEach { bottom = 0, left = 100, right = 100, top = 0 }, width fill ]
        (el
            [ height (px 1)
            , width fill
            , Background.color palette.color1
            ]
            none
        )



-- viewBlog : Blog -> Element msg
-- viewBlog blog =
--     let
--         title =
--             el [ Font.size 30, Font.semiBold, Font.center, width fill ] (text blog.title)
--
--         showFormattedText ftxt =
--             let
--                 colors =
--                     Array.fromList [ palette.neutral, palette.color1, palette.color2, palette.color3, palette.color4 ]
--
--                 fontAttrs =
--                     [ returnIf Font.semiBold ftxt.format.bold
--                     , returnIf Font.italic ftxt.format.cursive
--                     , returnIf Font.underline ftxt.format.underlined
--                     ]
--                         |> List.filterMap identity
--
--                 color =
--                     Array.get ftxt.format.color colors
--                         |> Maybe.withDefault palette.neutral
--                         |> Font.color
--             in
--             el (color :: fontAttrs) (text ftxt.text)
--
--         -- text ftxt.text
--         showLine ftxts =
--             row [] (List.map showFormattedText <| List.filter (\text -> not <| String.isEmpty text.text) ftxts)
--
--         splitLeave sep str =
--             let
--                 l =
--                     FormattedString.split sep str
--             in
--             List.indexedMap
--                 (\i x ->
--                     if i == List.length l - 1 then
--                         x
--
--                     else
--                         FormattedString.append x sep
--                 )
--                 l
--
--         lines ftxts =
--             List.map (splitLeave "\n") ftxts
--                 |> flatten
--                 |> List.filter (\str -> not (FormattedString.isEmpty str))
--                 |> splitAfter (\ftxt -> String.endsWith "\n" ftxt.text)
--                 |> List.map showLine
--
--         f elem =
--             case elem of
--                 Blog.SourceCode s ->
--                     row []
--                         [ el [ height fill, width (px 2), Background.color palette.color1 ] none
--                         , column [ Font.size 14, Font.family [ Font.monospace ], paddingXY 7 5, spacing 7 ] (lines s)
--                         ]
--
--                 Blog.Headline s ->
--                     el [ Font.size 22, Font.semiBold, width fill ] (text s)
--
--                 Blog.Section s ->
--                     paragraph [ spacing 8 ] [ text s ]
--
--                 Blog
--
--         list =
--             List.map f blog.elems
--
--         els =
--             [ title, divider ] ++ list
--     in
--     textColumn [ Font.size 16, width fill, height fill, paddingXY 100 40, spacing 20 ]
--         els


view :
    Data
    ->
        { path : Path
        , route : Maybe Route
        }
    -> Model
    -> (Msg -> msg)
    -> View msg
    -> { body : Html msg, title : String }
view sharedData page model toMsg pageView =
    { body =
        -- Element.layout [] (column [] (List.map html pageView.body))
        -- Html.div [ Attr.style "display" "flex", Attr.style "width" "100%", Attr.style "height" "fill" ]
            -- [ 
                Element.layout [ Background.color palette.bg, Font.color palette.neutral ]
                (column [ width fill, height fill ]
                    (navbar False :: pageView.body)
                 -- , viewBlog sharedData.blog
                )
            -- ]
    , title = pageView.title
    }
