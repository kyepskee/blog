module FormattedString exposing (..)


type alias Format =
    { color : Int
    , bold : Bool
    , cursive : Bool
    , underlined : Bool
    }


type alias FormattedString =
    { format : Format
    , text : String
    }


split : String -> FormattedString -> List FormattedString
split sep fstr =
    String.split sep fstr.text
        |> List.map (FormattedString fstr.format)


append fstr str =
    FormattedString fstr.format (fstr.text ++ str)


isEmpty fstr =
    String.isEmpty fstr.text
