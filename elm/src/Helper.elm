module Helper exposing (..)


paddingZero =
    { left = 0, right = 0, top = 0, bottom = 0 }


splitAfter : (a -> Bool) -> List a -> List (List a)
splitAfter f l =
    let
        helper : List (List a) -> List a -> List a -> List (List a)
        helper res cur ll =
            case ll of
                [] ->
                    res

                x :: xs ->
                    if f x then
                        helper (List.reverse (x :: cur) :: res) [] xs

                    else
                        helper res (x :: cur) xs
    in
    List.reverse (helper [] [] l)


flatten : List (List a) -> List a
flatten l =
    List.foldr (\acc ls -> List.append acc ls)
        []
        l


returnIf : a -> Bool -> Maybe a
returnIf value bool =
    if bool then
        Just value

    else
        Nothing
