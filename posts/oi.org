#+TITLE: Debugging tips for competitive programmers
#+SUMMARY: A few tips on how to debug simple bugs faster

* Debugging

Usually the process of solving a problem (during a contest) can be subdivided into three phases: coming up with a solution, implementing it and debugging it. Your goal as a contestant is to optimize all of those phases so they take up as little time as possible. Here are a few ways on how to (magically!) speed up the third phase.

Many competitive programmers use the simplest possible method: print statements. There are two types of bugs: bugs in the logic of your program (it returns a wrong answer, because you forgot to add a 1, for example) and bugs that throw an error (e.g the beloved "Segmentation fault").

* Assertions

For the first type of bug I have only one tip: use ~assert~ statements. The ~assert~ function takes one argument: a boolean value, and if the value is false it throws an error. How is that useful, you may ask. Well, it's a good way to check if your assumptions are true, and stay true under all circumstances.

While programming we always make some assumptions, some more complex than others. A good practice is to put all non-trivial assumptions in assert statements. This will not only catch more bugs during testing (as your program may return the correct answer while still being wrong somewhere), but also tell you what causes them.

This allows you to catch mistakes that stem from incorrect assumptions and bugs that cause one of your correct assumptions to fail. For example: if I think that a number will always be positive and divible by 3, I can put the following in my code.
#+BEGIN_SRC cpp
int main() {
    // ...
    assert(x > 0 && x % 3 == 0);
    // ...
}
#+END_SRC

When this fails, I know that either my code calculates ~x~ incorrectly, or my assumption about ~x~ was wrong. (This is a bit similar to "contract programming").

* valgrind

~valgrind~ is a tool everyone should start using now. The workflow most of us use is something like: write in a text editor, compile in a terminal and run the program in a terminal. Running the program in the terminal looks like this:
#+BEGIN_SRC sh
./main <in
#+END_SRC

Suppose that you have a bug, and this program throws a "Segmentation fault". Using ~valgrind~ tells us exactly which line of code throws the error (as long as you use the ~-g~ flag during compilation i.e ~g++ -g ...~).

#+BEGIN_SRC
valgrind ./main <in
#+END_SRC

At first, parsing ~valgrind~'s output might be hard, but you'll get used to it. And it's significantly faster than putting ~printf~ in random places, to see which line stops the execution of the program.

* Flags

Finally, one of the least known tools that you can use are some cool compiler flags, which can catch some errors for free!

C++ defines a lot of things as "undefined behavior". You generally shouldn't do anything that causes undefined behavior (for example accessing out-of-bounds elements in a ~std::vector~). The thing about UB is, even though it shouldn't happen, it won't cause your program to crash (in our case, we'd want it to crash, because then we know something's wrong and our programs have to be 100% correct, which is not the case for programming in general). For example, it might happen that you access an out-of-bounds element, but don't get an error. A general theme with these flags is making UB throw an error, which would otherwise go unnoticed.

The first flag is ~-D_GLIBCXX_DEBUG~, which enables the [[https://gcc.gnu.org/onlinedocs/libstdc++/manual/debug_mode.html][libstdc++ debug mode]]. In debug mode, most data structures from the standard library will signal errors instead of causing undefined behavior. For example, ~std::vector~ will throw an error on out-of-bounds accesses!
#+BEGIN_SRC cpp
int main() {
    vector<int> a = {1, 2, 3};
    cout << a[3] << endl;
    // Error: attempt to subscript container with out-of-bounds
    // index 3, but container only holds 3 elements.
    return 0;
}
#+END_SRC

The error does not tell you the line of code which caused the error, for that use the aforementioned ~valgrind~:
#+BEGIN_SRC
g++ -g -D_GLIBCXX_DEBUG main.cpp -o main && valgrind ./main <input
#+END_SRC

The other flag is ~-fsanitize=undefined~, which enables the [[https://clang.llvm.org/docs/UndefinedBehaviorSanitizer.html][UndefinedBehaviorSanitizer]]. UBSan is an undefined behavior detector, which will also catch many common types of undefined behavior such as:
+ Array out of bounds
+ *Signed* integer overflow (~int~, but not ~unsigned int~)
+ Dereferencing null pointers
and many others (the full list can be found in the site linked above). These are incredibly useful, especially finding overflows, which in some cases can be tough. The sanitizer does not halt the program when it finds an error by default. In most cases, you want the program to stop and exit with an error code - for that you also have to use ~fno-sanitize-recover~. Keep in mind that these flags don't work with the ~-static~ flag.

* Testing

I recommend adding ~set -e~ to the top of your testing script. This command causes your script to exit any time that any command inside the script exits with an error.
#+BEGIN_SRC bash
set -e

while true
do
    ./randomiser >input
    ./main <input >main_out
    ./brute <input >brute_out
    if diff -b main_out brute_out; then
        echo "Accepted"
    else
        echo "Wrong"
        exit 1
    fi
done
#+END_SRC

Sometimes you make a wrong assumption in both your main solution and brute force solution, which causes both solutions to exit with an error thus not producing any output. This will cause the script to treat it as the "Accepted" case, when you would want it to exit, too. ~set -e~ solves this problem, exiting whenever any of the solutions throws an error.

To provide an example, suppose both my solutions assumed that some value - let's say ~x~ - is always smaller than a 100. Both then initialize a ~vector~, which is indexed by ~x~.

#+BEGIN_SRC cpp
int main() {
    // ...
    // suppose this appears both in the main and brute force solution
    vector<int> a(100);
    a[x] *= 2; // ERROR: x is out-of-bounds
    // ...
    return 0;
}
#+END_SRC

Both ~brute~ and ~main~ throw segfaults and give empty outputs as a result. Our simple script treats this as "Accepted"! With ~set -e~, it will crash whenever either ~main~ or ~brute~ crashes, notifying us about the error.
